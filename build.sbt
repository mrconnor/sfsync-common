name := "sfsync-common"

version := "0.1.0-SNAPSHOT"

organization := "com.ariasystems"

scalaVersion := "2.11.8"

scalacOptions ++= Seq(
    "-Xmax-classfile-name", "100", // added to protect eCryptFS filename encryption
    "-deprecation",
    "-Ywarn-numeric-widen",
    "-encoding", "utf8",
    "-feature"
)

// This is required in order for publishing to Artifactory to work correctly
publishMavenStyle := true

// Here addition repository sites may be added. Minimally, our artifactory site is required.
resolvers ++=  Seq(
    "Aria" at "http://dvtools02.oqn.ariasystems.net:8080/artifactory/simple/libs-all-local/"
)

libraryDependencies ++= {
    lazy val versions = new {
        val spray = "1.3.2"
        val mockito = "2.1.0"
        val scalatest = "3.0.0"
        val scalamock = "3.2"
        val specs2 = "2.3.11"
        val authenticator = "1.0.0"
        val macwire = "2.2.5"
        val joda = "2.9.6"
        val enumeratum = "1.5.6"
    }

    Seq(
        // aria
        "com.aria.auth"  %% "authenticator" % versions.authenticator,

        // open source
        "io.spray"            %%  "spray-json"          % versions.spray,
        "com.softwaremill.macwire" %% "macros"          % versions.macwire % "provided",
        "joda-time"           % "joda-time"             % versions.joda,
        "com.beachape"        % "enumeratum_2.11"       % versions.enumeratum,

        // testing
        "io.spray"            %%  "spray-testkit"       % versions.spray  % "test",
        "org.specs2"          %%  "specs2-core"         % versions.specs2 % "test",
        "org.scalatest"       %% "scalatest"            % versions.scalatest % "test",
        "org.mockito"         % "mockito-core"          % versions.mockito % "test",
        "org.scalamock" %% "scalamock-scalatest-support" % versions.scalamock % "test"
    )
}

