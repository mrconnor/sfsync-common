package com.ariasystems.sfsync.common

import java.util.Date

import enumeratum._
import spray.json.{JsObject, _}


/**
  * Created by mconnor on 1/19/17.
  */

object CommonJsonProtocols extends DefaultJsonProtocol {

    /** Format generator for enums implemented using enumeratum.Enum.
      * @param base   the object that defines a particular enum.
      * @return  the JSON formatter for that enum
      */
    def enumJsonFormat[A<:EnumEntry](base: Enum[A]) = new JsonFormat[A] {
        def write(e: A): JsValue = JsString(e.toString)

        def read(value: JsValue): A = value match {
            case JsString(str) => base.withName(str)
            case _ => throw new DeserializationException("Enum value should be a JSON string")
        }
    }

    /**  Formatter for ISO 8601 formatted dates */
    val isoDateFormat = new JsonFormat[Date] {
        def write(date: Date): JsValue = {
            val jTime = new org.joda.time.DateTime(date)
            JsString(jTime.toString)
        }

        def read(value: JsValue): Date = value match {
            case JsString(str) =>
                val jTime = new org.joda.time.DateTime(str)
                jTime.toLocalDateTime().toDate
            case _ => throw new DeserializationException("Date should be JSON string in ISO 8601 format")
        }
    }


    // These two implicits allow us to convert generic property maps to and from JSON

    implicit object AnyJsonFormat extends JsonFormat[Any] {
        def write(x: Any) = x match {
            case n: Int => JsNumber(n)
            case n: Long => JsNumber(n)
            case s: String => JsString(s)
            case b: Boolean if b == true => JsTrue
            case b: Boolean if b == false => JsFalse
            case d: java.util.Date => isoDateFormat.write(d)
            case _@x => throw new UnsupportedDataMapValueType(x.getClass)
        }
        def read(value: JsValue) = value match {
            case JsNumber(n) => n.longValue()
            case JsString(s) => s
            case JsTrue => true
            case JsFalse => false
            case _ => throw new DeserializationException("Unsupported JSON structure for a data map entry")
        }
    }

    implicit val dataMapFormat = mapFormat[String, Any]

}

class UnsupportedDataMapValueType[T](clz: Class[T]) extends Exception(s"Value type: ${clz}")
