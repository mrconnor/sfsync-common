package com.ariasystems.sfsync.common

import java.util.Date

import enumeratum.{Enum, EnumEntry}

/**
  * Created by mconnor on 1/17/17.
  *
  * The ProvMgrEventRecord case class is the representation of ProvMgr generated
  * notification messages, as they are output into Kafka topics.
  */

case class ProvMgrEventRecord (
    request: RequestElement,
    event: EventElement
) {
    // convenience methods for common properties
    def eventDate = request.date
    def transactionId = event.event_id
    def eventType = event.event_type_id
    def clientNo = event.client_no
    def acctNo = event.acct_no
}

case class RequestElement (
    version: String,
    sender: String,
    date: Date
)

case class EventElement (
    event_id: Long,
    event_type_id: Int,
    action: String,
    _class: String,
    client_no: Long,
    acct_no: Long,
    acct_objects: List[AcctObject],
    client_objects: List[ClientObject]
)


case class AcctObject (
    object_type_id: ProvMgrObjectType,
    object_id: Long
)

case class ClientObject (
    object_type_id: Int,
    object_id: Long
)

sealed trait ProvMgrObjectType extends EnumEntry { val typeNo: Int }
object ProvMgrObjectType extends Enum[ProvMgrObjectType] {
    case object Unknown          extends ProvMgrObjectType { val typeNo = 0 }
    case object BillingGroup     extends ProvMgrObjectType { val typeNo = 1 }
    case object MPI              extends ProvMgrObjectType { val typeNo = 2 }
    case object PaymentMethod    extends ProvMgrObjectType { val typeNo = 3 }
    case object DunningGroup     extends ProvMgrObjectType { val typeNo = 4 }
    case object AcctSuppFields   extends ProvMgrObjectType { val typeNo = 5 }
    case object Contracts        extends ProvMgrObjectType { val typeNo = 6 }

    val values = findValues
}


import spray.json._

object ProvMgrEventRecordJsonProtocol extends DefaultJsonProtocol {

    // conversion of ProvMgrObjectType uses the number, not the name
    implicit val provMgrObjectTypeJsonFormat = new JsonFormat[ProvMgrObjectType] {
        def write(e: ProvMgrObjectType): JsValue = JsNumber(e.typeNo)

        def read(value: JsValue): ProvMgrObjectType = value match {
            case JsNumber(no) =>
                ProvMgrObjectType.values.find(_.typeNo == no).getOrElse(ProvMgrObjectType.Unknown)
            case _ =>
                throw new DeserializationException("Object type value")
        }
    }

    implicit val dateFormat = CommonJsonProtocols.isoDateFormat

    implicit val acctObjFormat = jsonFormat2(AcctObject)
    implicit val clientObjFormat = jsonFormat2(ClientObject)
    implicit val requestElementFormat = jsonFormat3(RequestElement)
    implicit val eventElementFormat = jsonFormat8(EventElement)
    implicit val inputRecordFormat = jsonFormat2(ProvMgrEventRecord)
}


