package com.ariasystems.sfsync.common

import enumeratum._
import spray.json._

/**
  * Created by mconnor on 1/17/17.
  *
  * The ObjectChangeDesc case class is the representation of what the Resolver
  * outputs into Kafka topics.  Each record describes a change to a particular
  * Aria object.
  *
  *
  */

case class ObjectChangeDesc (
    object_type: AriaObjectType,
    ids:        AriaObjectId,
    action:     ChangeAction,
    date:       java.util.Date,
    data:       Map[String, Any]
)


sealed trait AriaObjectType extends EnumEntry
object AriaObjectType extends Enum[AriaObjectType] {
    case object unknown  extends AriaObjectType
    case object client   extends AriaObjectType
    case object account  extends AriaObjectType
    case object contact  extends AriaObjectType
    case object plan     extends AriaObjectType
    case object mpi      extends AriaObjectType
    case object spi      extends AriaObjectType

    val values = findValues
}


trait AriaObjectId
case class ClientObjectId (client: Long) extends AriaObjectId
case class AcctObjectId (client: Long, acct: Long) extends AriaObjectId
case class ContactObjectId (client: Long, acct: Long, contact: Long) extends AriaObjectId
case class MPIObjectId (client: Long, acct: Long, mpi: Long) extends AriaObjectId
case class SPIObjectId (client: Long, acct: Long, spi: Long) extends AriaObjectId


sealed trait ChangeAction extends EnumEntry
object ChangeAction extends Enum[ChangeAction] {
    case object unknown   extends ChangeAction
    case object `new`     extends ChangeAction
    case object change    extends ChangeAction
    case object delete    extends ChangeAction

    val values = findValues
}



object ObjectChangeDescJsonProtocol extends DefaultJsonProtocol {
    import com.ariasystems.sfsync.common.CommonJsonProtocols._

    implicit val dateFormat = CommonJsonProtocols.isoDateFormat

    implicit val otypeFormat = CommonJsonProtocols.enumJsonFormat(AriaObjectType)
    implicit val actionFormat = CommonJsonProtocols.enumJsonFormat(ChangeAction)

    implicit val clientIdFormat = jsonFormat1(ClientObjectId)
    implicit val acctIdFormat = jsonFormat2(AcctObjectId)
    implicit val contactIdFormat = jsonFormat3(ContactObjectId)
    implicit val mpiIdFormat = jsonFormat3(MPIObjectId)
    implicit val spiIdFormat = jsonFormat3(SPIObjectId)

    implicit val objIdFormat = new JsonFormat[AriaObjectId] {
        override def read(json: JsValue): AriaObjectId = {
            val jobj = json.asJsObject
            jobj.fields match {
                case _@f if f.contains("mpi") => mpiIdFormat.read(json)
                case _@f if f.contains("spi") => spiIdFormat.read(json)
                case _@f if f.contains("contact") => contactIdFormat.read(json)
                case _@f if f.contains("acct") => acctIdFormat.read(json)
                case _@f if f.contains("client") => clientIdFormat.read(json)
            }
        }

        override def write(obj: AriaObjectId): JsValue = obj match {
            case cid:ClientObjectId   => clientIdFormat.write(cid)
            case aid:AcctObjectId     => acctIdFormat.write(aid)
            case cid:ContactObjectId  => contactIdFormat.write(cid)
            case mid:MPIObjectId      => mpiIdFormat.write(mid)
            case sid:SPIObjectId      => spiIdFormat.write(sid)
            case _ => throw new SerializationException(s"Unsupported aria object ID type: ${obj.getClass}")
        }
    }

    implicit val outRecFormat = new JsonFormat[ObjectChangeDesc] {
        val defaultFormatter = jsonFormat5(ObjectChangeDesc)

        override def read(json: JsValue): ObjectChangeDesc = defaultFormatter.read(json)

        // spray.json stopped insuring element order in a recent release.  That's technically
        // correct since JSON doesn't require element order in objects.  But this particular object
        // has a lot of visibility, and so formatting in the expected standard order is useful
        // for visual inspection.  Note that this implementation still does not insure any particular
        // element order for "ids" or "data" values.
        override def write(obj: ObjectChangeDesc): JsValue = {
            JsObject( scala.collection.immutable.ListMap(
                "objectType" -> obj.object_type.toJson,
                "ids" -> obj.ids.toJson,
                "action" -> obj.action.toJson,
                "date" -> obj.date.toJson,
                "data" -> obj.data.toJson
            ))
        }
    }

}

