package com.ariasystems.sfsync.common

import enumeratum.{Enum, EnumEntry}
import org.scalatest.{FeatureSpec, Matchers}
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue}

/**
  * Created by mconnor on 2/1/17.
  */
class JsonProtocolTest extends FeatureSpec with Matchers with DefaultJsonProtocol {
    import spray.json._

    feature("Enum conversion") {

        sealed trait TestEnum extends EnumEntry
        object TestEnum extends Enum[TestEnum] {

            case object Foo extends TestEnum
            case object Bar extends TestEnum
            case object BadaBoom extends TestEnum
            case object YaddaYadda extends TestEnum

            val values = findValues
        }

        implicit val x = CommonJsonProtocols.enumJsonFormat(TestEnum)


        scenario("Output to JSON") {
            val e1: TestEnum = TestEnum.BadaBoom
            val json1: JsValue = e1.toJson
            json1 shouldBe JsString("BadaBoom")

            val e2: TestEnum = TestEnum.Foo
            val json2: JsValue = e2.toJson
            json2 shouldBe JsString("Foo")

        }

        scenario("Input from JSON") {
            val json1 = JsString("Foo")
            val e1 = json1.convertTo[TestEnum]
            e1 shouldBe TestEnum.Foo

            val json2 = JsString("Bar")
            val e2 = json2.convertTo[TestEnum]
            e2 shouldBe TestEnum.Bar
        }
    }


    val testDateStr = "2017-01-23T12:24:13.607-08:00"
    val testDateEpoch = 1485203053607L

    feature("Json date conversions") {

        implicit val isoFormatter = CommonJsonProtocols.isoDateFormat

        scenario("Convert from ISO string to date") {
            val json = JsString(testDateStr)
            val dt: java.util.Date = json.convertTo[java.util.Date]
            dt.getTime should be(testDateEpoch)
        }

        scenario("Convert from date to ISO string") {
            val date = new java.util.Date(testDateEpoch)
            val json: JsValue = date.toJson
            json should be (JsString(testDateStr))
        }
    }


    feature("InputRecord formatting") {
        import DefaultJsonProtocol._
        import com.ariasystems.sfsync.common.ProvMgrEventRecordJsonProtocol._


        val testInput1: String =
            """{"request":{"version":"1","sender":"A","date":"%DATE%"},
        "event":{"event_id":6108,"event_type_id":737,"activity_date":"","action":"A","_class":"A","client_no":7000206,"acct_no":525,"invoice_no":null,
          "order_line_no":null,"financial_transaction_id":null,"email_message_id":null,"client_api_receipt_id":"","plan_instance_no":null,
          "acct_objects":[{"object_type_id":1,"object_id":63}],"client_objects":[]}}
    """.stripMargin.replace("%DATE%", testDateStr)


        scenario("Message isn't valid JSON") {

            def testException(text: String, exceptionMessage: String): Unit = {
                val json: JsValue = text.parseJson
                the [DeserializationException] thrownBy (json.convertTo[ProvMgrEventRecord]) should have message exceptionMessage
            }

            testException("""159""",
                "Object expected in field 'request'")

            testException("""{"test":123}""",
                "Object is missing required member 'request'")

            testException("""{"request":{"version":"1","sender":"X","date":"2017-4-1T1:2:3Z"}}""",
                "Object is missing required member 'event'")

            testException("""{"request":{"version":"1","sender":"X","date":"2017-4-1T1:2:3Z"},"event":{}}""",
                "Object is missing required member 'event_id'")

            // client_no is "abc", should be a number
            testException("""{"request":{"version":"1","sender":"X","date":"2017-4-1T1:2:3Z"},
                            |   "event":{"event_id":1,"event_type_id":1,"action":"X","_class":"X","client_no":"abc",
                            |   "acct_no":-22,"acct_objects":[],"client_objects":[]}}""".stripMargin,
                "Expected Long as JsNumber, but got \"abc\"")


            // Json parsing doesn't care about bad client numbers, only that they exist in the input
            val str = """{"request":{"version":"1","sender":"X","date":"2017-4-1T1:2:3Z"},
                        | "event":{"event_id":1,"event_type_id":1,"action":"X","_class":"X","client_no":-11,"acct_no":-22,"acct_objects":[],"client_objects":[]}}""".stripMargin
            val inrec = str.parseJson.convertTo[ProvMgrEventRecord]
            inrec.clientNo shouldBe -11
            inrec.acctNo shouldBe -22

        }

        scenario("Parse input json into InputRecord") {
            val json: JsValue = testInput1.parseJson
            val inputRecord: ProvMgrEventRecord = json.convertTo[ProvMgrEventRecord]

            inputRecord.request.version shouldBe "1"
            inputRecord.request.sender shouldBe "A"
            inputRecord.request.date.getTime shouldBe testDateEpoch

            inputRecord.event.event_id shouldBe 6108
            inputRecord.event._class shouldBe "A"
            inputRecord.event.event_type_id shouldBe 737
            inputRecord.event.client_no shouldBe 7000206
            inputRecord.event.acct_no shouldBe 525

            inputRecord.event.acct_objects should have length 1
            inputRecord.event.acct_objects(0).object_id shouldBe 63
            inputRecord.event.acct_objects(0).object_type_id shouldBe ProvMgrObjectType.BillingGroup
        }
    }


    feature("OutputRecord formatting") {

        scenario("Generate output json from OutputRecord") {
            import ObjectChangeDescJsonProtocol._

            val outputRecord = ObjectChangeDesc(
                AriaObjectType.mpi, MPIObjectId(12345, 98765, 412),
                ChangeAction.change, new java.util.Date(testDateEpoch),
                Map("name" -> "The Main Plan", "random_stat" -> 2593, "change_date" -> new java.util.Date(testDateEpoch))
            )

            // We should get exactly this as output, assuming compact (ie. not pretty) formatting
            val cleanOutput = s"""{"objectType":"mpi","ids":{"client":12345,"acct":98765,"mpi":412},"action":"change","date":"$testDateStr","data":{"name":"The Main Plan","random_stat":2593,"change_date":"$testDateStr"}}"""

            val json: JsValue = outputRecord.toJson
            val outstr: String = json.toString

            outstr shouldBe cleanOutput
        }
    }

}
